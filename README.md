# MIRA: Make It Run Already!

This (absolutly not serious) project uses [ChatGPT](https://chat.openai.com) API to produce correct Rust functions.

By "correct", we mean:
- compiling
- passing a test

Please take good note that nowhere in this list are the words "is doing what you asked it to do.

I repeat: for what we know, if you ask it to count to 10, this program may produce a function which will drop your tables or format your hard-drive. Consider yourself warned.

## How it works

It take a prompt from the user (you), adds ", in rust with a unit test." at the end, and send it to ChatGPT API.

Then it tries to compile the result.
If it doesn't compile, it forge a new prompt including the proposed function and the error message, and ask ChatGPT to fix it.

If it compiles, it tries to run the test.
If the test doesn't pass, it forge a new prompt including the proposed function and the error message, and ask ChatGPT to fix it.

rince, repeat until both steps pass, then print the resulting code.

## Compilation / Instalation

> cargo build --release

If you want to install it (not necessary):
> cargo install --path .

## Usage

You can either call it with:
> cargo run <prompt>

or:
> mira <prompt>

if it was installed

if no prompt is provided on the command line, it will read it from standard input.

It allways adds ", in rust with a unit test" a the end of your prompt.

You need to store your API_KEY either in an environement variable called "$GPT_API_KEY" or in a file .gpt_api_key in the current directory.

Good luck in the age of the rising machines.

## Example

```
$ mira "a function taking a text as argument and compute the ratio of occurence of each words in this text"
=== Make It Run Allready! ===

Starting prompt:
a function taking a text as argument and compute the ratio of occurence of each words in this text, in rust with a unit test.

=== Iteration: 0#
asking to (re)write function...
compiling...
code doesn't compile, iterating...

=== Iteration: 1#
asking to (re)write function...
compiling...
code compiles.
running tests...
tests fails, iterating...

=== Iteration: 2#
asking to (re)write function...
compiling...
code doesn't compile, iterating...

=== Iteration: 3#
asking to (re)write function...
compiling...
code compiles.
running tests...
tests fails, iterating...

=== Iteration: 4#
asking to (re)write function...
compiling...
code compiles.
running tests...
tests fails, iterating...

=== Iteration: 5#
asking to (re)write function...
compiling...
code compiles.
running tests...
Code works!
Here is the final code:


//Function to compute the ratio of occurence of each word in a text
fn compute_word_ratio(text: &str) -> Vec<(String, f64)> {
    use std::collections::HashMap;

    let mut word_count: HashMap<String, u32> = HashMap::new();
    let mut word_ratio: Vec<(String, f64)> = Vec::new();
    let mut total_words = 0;

    for word in text.split_whitespace() {
        *word_count.entry(word.to_string()).or_insert(0) += 1;
        total_words += 1;
    }

    for (word, count) in word_count.iter() {
        word_ratio.push((word.to_string(), *count as f64 / total_words as f64));
    }

    word_ratio.sort_by(|a, b| a.0.cmp(&b.0));

    word_ratio
}

//Unit test
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_word_ratio() {
        let text = "This is a test of the word ratio function";
        let expected_word_ratio: Vec<(String, f64)> = vec![("This".to_string(), 0.1111111111111111),
                                      ("a".to_string(), 0.1111111111111111),
                                      ("function".to_string(), 0.1111111111111111),
                                      ("is".to_string(), 0.1111111111111111),
                                      ("of".to_string(), 0.1111111111111111),
                                      ("ratio".to_string(), 0.1111111111111111),
                                      ("test".to_string(), 0.1111111111111111),
                                      ("the".to_string(), 0.1111111111111111),
                                      ("word".to_string(), 0.1111111111111111)];

        let word_ratio = compute_word_ratio(&text);
        assert_eq!(expected_word_ratio, word_ratio);
    }
}
```
