use std::error::Error;

#[derive(Debug)]
pub struct StupidError {
    details: String
}

impl StupidError {
    pub fn new(msg: &str) -> Self {
        Self { details: msg.to_string() }
    }
}

impl std::fmt::Display for StupidError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f,"{}",self.details)
    }
}

impl Error for StupidError {
    fn description(&self) -> &str {
        &self.details
    }
}
