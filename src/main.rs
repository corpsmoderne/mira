mod error;

//use reqwest::header;
use reqwest::header::{HeaderValue,HeaderMap};
use serde_json::Value;
use std::fs::File;
use std::io::prelude::*;
use std::process::{Command,Stdio};
use std::env;
use error::StupidError;
use std::error::Error;

static TMP_SRC : &str = "/tmp/gpt_rust.rs";
static TMP_BIN : &str = "/tmp/gpt_rust";

#[derive(Debug,Clone)]
struct Job {
    initial_prompt: String,
    current_prompt: String,
    api_key: String,
}

impl Job {
    fn new(prompt: String, api_key: String) -> Self {
	Job { initial_prompt: prompt.clone(),
	      current_prompt: prompt,
	      api_key
	}
    }
}
    
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    println!("=== Make It Run Allready! ===\n");

    let mut job = make_job()?;
    
    println!("Starting prompt:\n{}\n", job.initial_prompt);

    let mut iter = 0;    
    loop {
        println!("=== Iteration: {iter}#");
        let res = single_round(&job).await?;
        
        match res {
            Some(new_prompt) => job.current_prompt = new_prompt,
            None => break
        }
        iter += 1;
        println!();
    }
    Ok(())
}

fn make_job() -> Result<Job, Box<dyn Error>> {
    let api_key = get_api_key()
	.ok_or_else(|| StupidError::new("GPT3_API_KEY env var not found,\
 neither was .gpt3_api_key file"))?;
    
    let mut prompt = env::args().nth(1)
	.or_else(|| {
	    println!("No args provided, reading prompt from stdin:");
	    let mut input = String::new();
	    std::io::stdin().read_to_string(&mut input).ok()?;
	    Some(input)
	})
	.expect("we need one argument as a prompt, \
		 or text in the standard input");
    prompt.push_str(", in rust with a unit test.");

    Ok(Job::new(prompt, api_key))
}

async fn single_round(job: &Job) -> Result<Option<String>, reqwest::Error>
{
    println!("asking to (re)write function...");
    let fct = send_req(&job.current_prompt, job.api_key.trim()).await?;
    
    //println!("==== Solution suggested:");
    //println!("{}", fct);
    //println!("====");

    let mut file = File::create(TMP_SRC).expect("can't create file");
    file.write_all(&fct.bytes().collect::<Vec<u8>>())
        .expect("can't write in file");

    let child = Command::new("rustc")
        .arg("--crate-type=lib")
        .arg("--test")
        .arg("-A")
        .arg("dead_code")
        .arg("-o")
        .arg(TMP_BIN)
        .arg(TMP_SRC)
        .stdin(Stdio::null())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().expect("can't run command");

    println!("compiling...");

    let output = child.wait_with_output().expect("no output");

    let stderr = String::from_utf8(output.stderr).unwrap();
    //if !stderr.is_empty() {
    //    println!("StdErr>>> {}", &stderr);
    //}
    
    match output.status.code() {
        Some(0)=> println!("code compiles.") ,
        None => panic!("Not sure what's going on here..."),
        Some(_) => {
            println!("code doesn't compile, iterating...");
            let output = format!("This rust function doesn't compile: \
                                  {} \nThe compiler is giving me this \
                                  error:\n{}\ncan you give me the \
                                  corrected code?",
                                 fct,
                                 stderr);
            return Ok(Some(output));
        }
    }

    // run unit tests...
    let child = Command::new(TMP_BIN)
        .stdin(Stdio::null())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn().expect("can't run command");

    println!("running tests...");
    
    let output = child.wait_with_output().expect("no output");

    let stdout = String::from_utf8(output.stdout).unwrap();
    //if !stderr.is_empty() {
    //    println!("StdErr>>> {}", &stderr);
    //}
    
    match output.status.code() {
        Some(0)=> println!("Code works!\nHere is the final code:\n{}", fct),
        _ => {
            println!("tests fails, iterating...");
            let output = format!("This rust function doesn't work: \
                                  {} \nThe tests included fails with \
                                  the error:{}\ncan you give me the \
                                  corrected code?",
                                 fct,
                                 stdout);
            return Ok(Some(output));
        }
    }
    
    Ok(None)
}

async fn send_req(
    prompt: &str,
    api_key: &str
) -> Result<String, reqwest::Error> {
    let bearer = format!("Bearer {}", api_key);
    let mut headers = HeaderMap::new();
    headers.insert("Content-Type",
                   HeaderValue::from_static("application/json"));
    headers.insert("Authorization",
                   HeaderValue::from_str(&bearer).unwrap());
    
    let client = reqwest::Client::builder()
        .default_headers(headers)
        .build()?;
    let res : serde_json::Value =
        client.post("https://api.openai.com/v1/completions")
        .json(&serde_json::json!({
            "model": "text-davinci-003",
            "temperature": 0.5,
            "max_tokens": 2048,
            "prompt": String::from(prompt)
        }))
        .send()
        .await?
        .json()
        .await?;        

    let Value::Array(choices) = res.get("choices")
        .unwrap_or_else(|| panic!("no choices: {}",
				  serde_json::to_string_pretty(&res)
				  .unwrap())) else {
            panic!("expected an Array");
        };
    let Some(Value::String(fct)) = choices
        .iter().next()
        .expect("no solution found :(").get("text") else {
            panic!("expected an Some String");
        };

    Ok(fct.to_string())
}

fn get_api_key() -> Option<String> {
    env::var("GPT3_API_KEY").ok().or_else(|| {
	std::fs::read_to_string(".gpt3_api_key").ok()
    })
}
